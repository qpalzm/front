import axios from "axios";
import qs from 'querystring';

const axiosInstance = axios.create({
    baseURL: '/',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': `Basic dGFzazAyOm15LXNlY3JldC1rZXk=` // Генерируем ключ для бэка
    }
});

export const authAPI = {
    async getToken(username, password) {
        const queryString = qs.stringify({
            grant_type: 'password',
            username,
            password,
        })
        return axiosInstance.post(`/api/oauth/token`, queryString)
            .then(response => response.data);
    }
}
