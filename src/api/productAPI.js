import getAxiosInstance from "./axiosInstance";

export const productAPI = {
    async getGraph(clusterId, date) {
        const axiosInstance = await getAxiosInstance();
        return axiosInstance.get(`/api/graph?id=${clusterId}${date ? `&date=${date}T00:00` : ''}`)
            .then(response => response.data);
    },
    async getClusters() {
        const axiosInstance = await getAxiosInstance();
        return axiosInstance.get(`/api/clusters`)
            .then(response => response.data);
    },
    async getGroups(clusterId) {
        const axiosInstance = await getAxiosInstance();
        return axiosInstance.get(`/api/clusters/${clusterId}/groups`)
            .then(response => response.data);
    },
    async addProduct(product) {
        const axiosInstance = await getAxiosInstance();
        return axiosInstance.put(`/api/products`, product)
            .then(response => response.data);
    },
    async setProduct(product) {
        const axiosInstance = await getAxiosInstance();
        return axiosInstance.post(`/api/products`, product)
            .then(response => response.data);
    },
    async deleteProduct(productId) {
        const axiosInstance = await getAxiosInstance();
        return axiosInstance.delete(`/api/products/${productId}`)
            .then(response => response.data);
    },
    async getProducts(clusterId, groupId) {
        const axiosInstance = await getAxiosInstance();
        return axiosInstance.get(`/api/clusters/${clusterId}/groups/${groupId}/products`)
            .then(response => response.data);
    },
    async setLink(childId, parentId, childType) {
        const axiosInstance = await getAxiosInstance();
        return axiosInstance.post(`/api/graph/edges/`,{
            edgeUpdateRequests: [{
                childType,
                edges: [{childId,parentId}]
            }]
        })
            .then(response => response.data);
    },
}
