import axios from "axios";

function getAxiosInstance() {
    const token = localStorage.getItem('token')

    return axios.create({
        baseURL: '/',
        withCredentials: true,
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
        }
    });
}

export default getAxiosInstance;
