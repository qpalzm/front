import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {addComment} from "../../redux/action/comments";
import "./comments.css";
import Comment from "./Comment";
import ReactPaginate from 'react-paginate';

const CommentList = (props) => {

    const dispatch = useDispatch();

    const [page, setPage] = useState(0);
    const {comments, pageCount} = useSelector(state => state.commentsData);
    const [message, setMessage] = useState("");

    const handleChangeMessage = (evt) => {
        setMessage(evt.target.value)
    }

    const handleClickPage = (page) => {
        setPage(page.selected)
    }
    const handleAdd = () => {
        dispatch(addComment(message))
        setMessage("")
    }
    const handleCloseComments = () => {
        props.handleCloseComments()
    }

    return (
        <div className="comments-block">
            <h3 className="comments-title">Список комментариев</h3>
            <div className="comments-close-btn" aria-label="Close" onClick={handleCloseComments}>&#215;</div>
            <div className="comments-add-block">
                <div className="comments-add-block-title">
                    Добавить комментарий
                </div>
                <input
                    type="text"
                    className="comment-edit-field"
                    onChange={handleChangeMessage}
                    value={message}
                />
                <button className="comment-edit-save" onClick={handleAdd} disabled={!message}>Сохранить</button>
            </div>
            {comments.length ?
                <>
                    <div className="comments-list">
                        {comments.map((comment, index) => {
                            if (index >= page * 10 && index < (page + 1) * 10) {
                                return (
                                    <Comment key={comment.id} comment={comment}/>
                                )
                            }
                            return <React.Fragment key={index} />
                        })}
                    </div>
                    <div className="pagination">
                        <ReactPaginate
                            previousLabel={'<'}
                            nextLabel={'>'}
                            breakLabel={'...'}
                            breakClassName={'break-me'}
                            pageCount={pageCount}
                            marginPagesDisplayed={2}
                            pageRangeDisplayed={5}
                            onPageChange={handleClickPage}
                            containerClassName={'pagination-list'}
                            activeClassName={'active'}
                        />
                    </div>
                </>
                :
                <div className="comments-list-empty">Список комментариев пуст</div>
            }
        </div>
    )
}

export default CommentList;
