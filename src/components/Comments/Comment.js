import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {deleteComment, setComment} from "../../redux/action/comments";
import "./comments.css";

const CommentList = ({comment}) => {

    const dispatch = useDispatch();

    const [isEdit, setIsEdit] = useState(false);
    const [message, setMessage] = useState("");

    const handleEdit = () => {
        setIsEdit(true)
        setMessage(comment.message)
    }

    const handleDelete = () => {
        dispatch(deleteComment(comment.id))
    }

    const handleChangeMessage = (evt) => {
        setMessage(evt.target.value)
    }

    const handleSave = () => {
        dispatch(setComment({
            ...comment,
            message
        }))
        setIsEdit(false)
    }

    const handleCancelMessageEdit = () => {
        setIsEdit(false)
    }

    return (
        <div className="comment">
            <div className="comment-username">
                {comment.username}
            </div>
            <div className="comment-date">
                {comment.date}
            </div>
            <div className="comment-message">
                {isEdit ?
                    <>
                        <input
                            type="text"
                            className="comment-edit-field"
                            onChange={handleChangeMessage}
                            value={message}
                        />
                        <button className="comment-edit-save" onClick={handleSave} disabled={!message}>Сохранить</button>
                        <button className="comment-edit-cancel" onClick={handleCancelMessageEdit}>Отмена</button>
                    </>
                    :
                    <>{comment.message}</>
                }
            </div>
            {!isEdit &&
            <>
                <div className="icon icon-delete" onClick={handleDelete}/>
                <div className="icon icon-edit" onClick={handleEdit}/>
            </>
            }

        </div>
    )
}

export default CommentList;
