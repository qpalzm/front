import React, {useState} from 'react';
import {useDispatch} from 'react-redux';
import "./Login.css"
import {getToken} from "../../redux/function/auth";

const Login = () => {

    const dispatch = useDispatch();

    const [username, setUsername] = useState();
    const [password, setPassword] = useState();

    const handleChangeUsername = (evt) => {
        setUsername(evt.target.value)
    }

    const handleChangePassword = (evt) => {
        setPassword(evt.target.value)
    }

    const handleLogin = (evt) => {
        dispatch(getToken(username, password))
        evt.preventDefault()
        return false
    }

    return (
        <form className="loginForm" onSubmit={handleLogin}>
            <h2>Авторизация</h2>
            <div>
                <label>Имя пользователя</label>
                <input
                    type='text'
                    name='username'
                    placeholder='Имя пользователя'
                    value={username}
                    onChange={handleChangeUsername}
                />
            </div>
            <div>
                <label>Пароль</label>
                <input
                    type='password'
                    name='password'
                    placeholder='Пароль'
                    value={password}
                    onChange={handleChangePassword}
                />
            </div>
            <div className='btnLine'>
                <button className='loginBtn' onClick={handleLogin}>Войти</button>
            </div>
        </form>
    )
}

export default Login
