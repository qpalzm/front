/* eslint-disable no-undef */
import React, {useEffect, useRef, useState} from 'react';
import * as d3 from "d3";
import './foundation.css'
import './Dendrogram.css'
import {addProduct, deleteProduct, getClusters, getGraph, setLink, setProduct} from "../../redux/function/products"
import {useDispatch, useSelector} from "react-redux";
import CommentList from "../Comments/CommentList";

const Dendrogram = () => {

    const dispatch = useDispatch();

    const [isOpenComment, setIsOpenComment] = useState(false)
    const [isOpenCreateNode, setIsOpenCreateNode] = useState(false)
    const [isOpenRenameNode, setIsOpenRenameNode] = useState(false)
    const [createName, setCreateName] = useState("")
    const [rename, setRename] = useState("")

    const [nodeRename, setNodeRename] = useState(null)
    const [createNodeParent, setCreateNodeParent] = useState(null)
    const [createNodeModalActive, setCreateNodeModalActive] = useState(null)
    const [renameNodeModalActive, setRenameNodeModalActive] = useState(null)

    const [clusterId, setClusterId] = useState(0)
    const [date, setDate] = useState("")

    const graph = useSelector(state => state.productData.graph)
    const clusters = useSelector(state => state.productData.clusters)

    const outer_update = useRef()

    const generateUUID = () => {
        let d = new Date().getTime();
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function () {
            let r = (d + Math.random() * 16) % 16 | 0;
            return (r);
        });
    }

    const rename_node = () => {
        if (nodeRename && renameNodeModalActive) {
            nodeRename.name = rename;
            setRenameNodeModalActive(false);
        }
        setIsOpenRenameNode(false);
        dispatch(setProduct({
            id: nodeRename.id,
            name: nodeRename.name,
            type: nodeRename.type,
            parentId: nodeRename.parent.ID
        }))
        outer_update.current(nodeRename);
    }

    const create_node = () => {
        if (createNodeParent && createNodeModalActive) {
            if (createNodeParent._children != null) {
                createNodeParent.children = createNodeParent._children;
                createNodeParent._children = null;
            }
            if (createNodeParent.children == null) {
                createNodeParent.children = [];
            }
            const id = generateUUID();
            const name = createName;

            dispatch(addProduct({
                name: name,
                parentId: createNodeParent.ID
            }))
                .then(newNode => {
                    const new_node = {
                        name: name,
                        id: id,
                        depth: createNodeParent.depth + 1,
                        type: "PRODUCT",
                        ID: newNode.id,
                        children: [],
                        '_children': undefined
                    };
                    createNodeParent.children.push(new_node);
                    setCreateNodeModalActive(false);
                    setCreateName("")

                    setIsOpenCreateNode(false)
                    outer_update.current(createNodeParent);
                })

        }
    }

    useEffect(() => {
        d3.select("svg").remove();

        d3.contextMenu = function (menu, openCallback) {

            d3.selectAll('.d3-context-menu').data([1])
                .enter()
                .append('div')
                .attr('class', 'd3-context-menu');

            d3.select('body').on('click.d3-context-menu', function () {
                d3.select('.d3-context-menu').style('display', 'none');
            });

            return function (data, index) {
                const elm = this;
                const currentMenu = [];

                menu.forEach(menuItem => {
                    if (menuItem.typesObject.includes(data.type)) {
                        currentMenu.push(menuItem)
                    }
                })
                if (currentMenu.length === 0) {
                    return
                }

                d3.selectAll('.d3-context-menu').html('');
                let list = d3.selectAll('.d3-context-menu').append('ul');
                list.selectAll('li').data(currentMenu).enter()
                    .append('li')
                    .html(function (d) {
                        return (typeof d.title === 'string') ? d.title : d.title(data);
                    })
                    .on('click', function (d) {
                        d.action(elm, data, index);
                        d3.select('.d3-context-menu').style('display', 'none');
                    });

                if (openCallback) {
                    if (openCallback(data, index) === false) {
                        return;
                    }
                }

                d3.select('.d3-context-menu')
                    .style('left', (d3.event.pageX - 2) + 'px')
                    .style('top', (d3.event.pageY - 2) + 'px')
                    .style('display', 'block');

                d3.event.preventDefault();
                d3.event.stopPropagation();
            };
        };

        if (graph !== null) {


            let maxLabelLength = 0;
            // значения для drag/drop
            let selectedNode = null;
            let draggingNode = null;

            const panSpeed = 200;
            const panBoundary = 20;

            let i = 0;
            const duration = 750;
            let root;

            // Размеры диаграммы
            const viewerWidth = document.documentElement.clientWidth;
            const viewerHeight = document.documentElement.clientHeight;

            let panTimer = null,
                node = null,
                dragListener = null,
                x = null,
                y = null,
                scale = null,
                domNode = null,
                relCoords = null,
                translateX = null,
                translateY = null,
                translateCoords = null,
                nodes = null,
                links = null,
                dragStarted = null;

            let tree = d3.layout.tree()
                .size([viewerHeight, viewerWidth]);

            // определение d3 диагональной проекции
            let diagonal = d3.svg.diagonal()
                .projection(function (d) {
                    return [d.y, d.x];
                });

            let menu = [
                {
                    typesObject: ["PRODUCT"],
                    title: 'Переименовать узел',
                    action: function (elm, d) {
                        setRename(d.name)
                        setRenameNodeModalActive(true);
                        setIsOpenRenameNode(true);
                        setNodeRename(d)
                    }
                },
                {
                    typesObject: ["PRODUCT"],
                    title: 'Удалить узел',
                    action: function (elm, d) {
                        delete_node(d);
                    }
                },
                {
                    typesObject: ["PRODUCT_GROUP"],
                    title: 'Создать дочерний узел',
                    action: function (elm, d) {
                        setCreateNodeParent(d);
                        setCreateNodeModalActive(true);
                        setIsOpenCreateNode(true)
                    }
                }
            ]

            // Рекурсивная вспомогательная функция для выполнения некоторых настроек путем обхода всех узлов
            function visit(parent, visitFn, childrenFn) {
                if (!parent) return;

                visitFn(parent);

                let children = childrenFn(parent);
                if (children) {
                    let count = children.length;
                    for (let i = 0; i < count; i++) {
                        visit(children[i], visitFn, childrenFn);
                    }
                }
            }

            // Вызов функции посещения всех узлов, чтобы установить maxLabelLength
            visit(graph, function (d) {
                maxLabelLength = Math.max(d.name.length, maxLabelLength);

            }, function (d) {
                return d.children && d.children.length > 0 ? d.children : null;
            });

            function delete_node(node) {
                visit(graph, function (d) {
                        if (d.children) {
                            for (let child of d.children) {
                                if (child === node) {

                                    d.children = _.without(d.children, child);
                                    update(root);
                                    dispatch(deleteProduct(node.ID))
                                    break;
                                }
                            }
                        }
                    },
                    function (d) {
                        return d.children && d.children.length > 0 ? d.children : null;
                    });
            }


            // сортировка дерева по именам узлов
            function sortTree() {
                tree.sort(function (a, b) {
                    return b.name.toLowerCase() < a.name.toLowerCase() ? 1 : -1;
                });
            }

            sortTree();

            function pan(domNode, direction) {
                let speed = panSpeed;
                if (panTimer) {
                    clearTimeout(panTimer);
                    translateCoords = d3.transform(svgGroup.attr("transform"));
                    if (direction === 'left' || direction === 'right') {
                        translateX = direction === 'left' ? translateCoords.translate[0] + speed : translateCoords.translate[0] - speed;
                        translateY = translateCoords.translate[1];
                    } else if (direction === 'up' || direction === 'down') {
                        translateX = translateCoords.translate[0];
                        translateY = direction === 'up' ? translateCoords.translate[1] + speed : translateCoords.translate[1] - speed;
                    }
                    scale = zoomListener.scale();
                    svgGroup.transition().attr("transform", "translate(" + translateX + "," + translateY + ")scale(" + scale + ")");
                    d3.select(domNode).select('g.node').attr("transform", "translate(" + translateX + "," + translateY + ")");
                    zoomListener.scale(zoomListener.scale());
                    zoomListener.translate([translateX, translateY]);
                    panTimer = setTimeout(function () {
                        pan(domNode, speed, direction);
                    }, 50);
                }
            }

            // Функция масштабирования
            function zoom() {
                svgGroup.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
            }

            // определение zoomListener, который вызывает функцию масштабирования для события «zoom», ограниченного в пределах scaleExtents
            let zoomListener = d3.behavior.zoom().scaleExtent([0.1, 3]).on("zoom", zoom);

            function initiateDrag(d, domNode) {
                draggingNode = d;
                d3.select(domNode).select('.ghostCircle').attr('pointer-events', 'none');
                d3.selectAll('.ghostCircle').attr('class', function (elem) {
                    return `ghostCircle ${elem.depth === d.parent.depth && d.parent.depth === 1 ? "show" : ""}`
                });
                d3.select(domNode).attr('class', 'node activeDrag');

                svgGroup.selectAll("g.node").sort(function (a) { // выберите родителя и отсортируйте путь
                    if (a.id !== draggingNode.id) return 1; // a не зависший элемент, отправьте "a" назад
                    else return -1; // a - элемент, на который наведен курсор, вывести "a" на передний план
                });
                // если у узлов есть дочерние элементы, удалить ссылки и узлы
                if (nodes.length > 1) {
                    // удалить пути ссылок
                    links = tree.links(nodes);
                    svgGroup.selectAll("path.link")
                        .data(links, function (d) {
                            return d.target.id;
                        }).remove();
                    // удалить дочерние узлы
                    svgGroup.selectAll("g.node")
                        .data(nodes, function (d) {
                            return d.id;
                        }).filter(function (d) {
                            return d.id !== draggingNode.id;

                        }).remove();
                }

                // удалить родительскую ссылку
                tree.links(tree.nodes(draggingNode.parent));
                svgGroup.selectAll('path.link').filter(function (d) {
                    return d.target.id === draggingNode.id;

                }).remove();

                dragStarted = null;
            }

            // определение baseSvg, прикрепив класс для стилизации и zoomListener
            let baseSvg = d3.select("#tree-container").append("svg")
                .attr("width", viewerWidth)
                .attr("height", viewerHeight)
                .attr("class", "overlay")
                .call(zoomListener);

            baseSvg.append("rect")
                .attr("width", "100%")
                .attr("height", "100%")
                .attr("fill", "white")


            // Определите слушателей drag/drop для поведения перетаскивания узлов.
            dragListener = d3.behavior.drag()
                .on("dragstart", function (d) {
                    if (d === root) {
                        return;
                    }
                    dragStarted = true;
                    nodes = tree.nodes(d);
                    d3.event.sourceEvent.stopPropagation();
                })
                .on("drag", function (d) {
                    if (d === root) {
                        return;
                    }
                    if (dragStarted) {
                        domNode = this;
                        initiateDrag(d, domNode);
                    }

                    // получить координаты mouseEvent относительно контейнера svg, чтобы разрешить панорамирование
                    let svg = document.getElementsByTagName('svg')[0];
                    relCoords = d3.mouse(svg);
                    if (relCoords[0] < panBoundary) {
                        panTimer = true;
                        pan(this, 'left');
                    } else if (relCoords[0] > (svg.clientWidth - panBoundary)) {

                        panTimer = true;
                        pan(this, 'right');
                    } else if (relCoords[1] < panBoundary) {
                        panTimer = true;
                        pan(this, 'up');
                    } else if (relCoords[1] > (svg.clientHeight - panBoundary)) {
                        panTimer = true;
                        pan(this, 'down');
                    } else {
                        try {
                            clearTimeout(panTimer);
                        } catch (e) {

                        }
                    }

                    d.x0 += d3.event.dy;
                    d.y0 += d3.event.dx;
                    let node = d3.select(this);
                    node.attr("transform", "translate(" + d.y0 + "," + d.x0 + ")");
                    updateTempConnector();
                })
                .on("dragend", function (d) {
                    if (d === root) {
                        return;
                    }
                    domNode = this;
                    if (selectedNode) {
                        // удаление элемента из родительского элемента и вставка его в новые дочерние элементы
                        let index = draggingNode.parent.children.indexOf(draggingNode);
                        if (index > -1) {
                            draggingNode.parent.children.splice(index, 1);
                        }
                        if (selectedNode.children || selectedNode._children) {
                            if (typeof selectedNode.children !== 'undefined') {
                                selectedNode.children.push(draggingNode);
                            } else {
                                selectedNode._children.push(draggingNode);
                            }
                        } else {
                            selectedNode.children = [];
                            selectedNode.children.push(draggingNode);
                        }

                        if (draggingNode?.ID && selectedNode?.ID) {
                            dispatch(setLink(draggingNode.ID, selectedNode.ID, draggingNode.type));
                        }

                        expand(selectedNode);
                        sortTree();
                        endDrag();
                    } else {
                        endDrag();
                    }
                });

            function endDrag() {
                selectedNode = null;
                d3.selectAll('.ghostCircle').attr('class', 'ghostCircle');
                d3.select(domNode).attr('class', 'node');
                d3.select(domNode).select('.ghostCircle').attr('pointer-events', '');
                updateTempConnector();
                if (draggingNode !== null) {
                    update(root);
                    centerNode(draggingNode);
                    draggingNode = null;
                }
            }

            function expand(d) {
                if (d._children) {
                    d.children = d._children;
                    d.children.forEach(expand);
                    d._children = null;
                }
            }

            let overCircle = function (d) {
                selectedNode = d;
                updateTempConnector();
            };
            let outCircle = function () {
                selectedNode = null;
                updateTempConnector();
            };

            // Функция обновления временного коннектора с указанием принадлежности перетаскивания
            let updateTempConnector = function () {
                let data = [];
                if (draggingNode !== null && selectedNode !== null) {
                    // необходимо перевернуть исходные координаты, поскольку мы сделали это для существующих соединителей в исходном дереве
                    data = [{
                        source: {
                            x: selectedNode.y0,
                            y: selectedNode.x0
                        },
                        target: {
                            x: draggingNode.y0,
                            y: draggingNode.x0
                        }
                    }];
                }
                let link = svgGroup.selectAll(".templink").data(data);

                link.enter().append("path")
                    .attr("class", "templink")
                    .attr("d", d3.svg.diagonal())
                    .attr('pointer-events', 'none');

                link.attr("d", d3.svg.diagonal());

                link.exit().remove();
            };

            // Функция центрирования узла при clicked/dropped, чтобы узел не терялся при collapsing/moving с большим количеством дочерних элементов.
            function centerNode(source) {
                scale = zoomListener.scale();
                x = -source.y0;
                y = -source.x0;
                x = x * scale + viewerWidth / 2;
                y = y * scale + viewerHeight / 2;
                d3.select('g').transition()
                    .duration(duration)
                    .attr("transform", "translate(" + x + "," + y + ")scale(" + scale + ")");
                zoomListener.scale(scale);
                zoomListener.translate([x, y]);
            }

            // Функция переключения дочерних узлов
            function toggleChildren(d) {
                if (d.children) {
                    d._children = d.children;
                    d.children = null;
                } else if (d._children) {
                    d.children = d._children;
                    d._children = null;
                }
                return d;
            }

            // Функция переключения дочерних узлов по клику
            function click(d) {
                if (d3.event.defaultPrevented) return;
                d = toggleChildren(d);
                update(d);
                centerNode(d);
            }

            function update(source) {
                // Вычислить новую высоту, функция подсчитывает общее количество потомков корневого узла и соответственно устанавливает высоту дерева.
                // Это предотвращает сплющенный вид макета, когда новые узлы становятся видимыми, или разреженный вид при удалении узлов
                // Это делает макет более согласованным.
                let levelWidth = [1];
                let childCount = function (level, n) {

                    if (n.children && n.children.length > 0) {
                        if (levelWidth.length <= level + 1) levelWidth.push(0);

                        levelWidth[level + 1] += n.children.length;
                        n.children.forEach(function (d) {
                            childCount(level + 1, d);
                        });
                    }
                };
                childCount(0, root);
                let newHeight = d3.max(levelWidth) * 35; // 25 пикселей в строке
                tree = tree.size([newHeight, viewerWidth]);

                // Вычислить новый макет дерева.
                let nodes = tree.nodes(root).reverse(),
                    links = tree.links(nodes);

                // Устанавливаем ширину между уровнями на основе maxLabelLength.
                nodes.forEach(function (d) {
                    d.y = (d.depth * (maxLabelLength * 10)); //maxLabelLength * 10px
                    // alternatively to keep a fixed scale one can set a fixed depth per level
                    // Normalize for fixed-depth by commenting out below line
                    // d.y = (d.depth * 500); //500px per level.
                });

                // Обновление узлов
                node = svgGroup.selectAll("g.node")
                    .data(nodes, function (d) {
                        return d.id || (d.id = ++i);
                    });

                // Введите любые новые узлы в предыдущую позицию родителя.
                let nodeEnter = node.enter().append("g")
                    .call(dragListener)
                    .attr("class", "node")
                    .attr("depth", function (d) {
                        return d.depth
                    })
                    .attr("transform", function () {
                        return "translate(" + source.y0 + "," + source.x0 + ")";
                    })
                    .on('click', click);

                nodeEnter.append("circle")
                    .attr('class', 'nodeCircle')
                    .attr("r", 0)
                    .style("fill", function (d) {
                        return d._children ? "lightsteelblue" : "#fff";
                    });

                nodeEnter.append("text")
                    .attr("x", function (d) {
                        return d.children || d._children ? -10 : 10;
                    })
                    .attr("dy", ".35em")
                    .attr('class', 'nodeText')
                    .attr("text-anchor", function (d) {
                        return d.children || d._children ? "end" : "start";
                    })
                    .text(function (d) {
                        return d.name;
                    })
                    .style("fill-opacity", 0);

                // фантомный узел, чтобы дать нам навести курсор мыши в радиусе вокруг него
                nodeEnter.append("circle")
                    .attr('class', 'ghostCircle')
                    .attr("r", 30)
                    .attr("opacity", 0.2)
                    .style("fill", "red")
                    .attr('pointer-events', 'mouseover')
                    .on("mouseover", function (node) {
                        overCircle(node);
                    })
                    .on("mouseout", function (node) {
                        outCircle(node);
                    });

                node.select('text')
                    .attr("x", function (d) {
                        return d.children || d._children ? -10 : 10;
                    })
                    .attr("text-anchor", function (d) {
                        return d.children || d._children ? "end" : "start";
                    })
                    .text(function (d) {
                        return d.name;
                    });

                // Измените заливку круга в зависимости от того, есть ли у него дочерние элементы и он свернут
                node.select("circle.nodeCircle")
                    .attr("r", 4.5)
                    .style("fill", function (d) {
                        return d._children ? "lightsteelblue" : "#fff";
                    });

                // Add a context menu
                node.on('contextmenu', d3.contextMenu(menu))

                // Перемещение узлов в новое положение.
                let nodeUpdate = node.transition()
                    .duration(duration)
                    .attr("transform", function (d) {
                        return "translate(" + d.y + "," + d.x + ")";
                    });

                nodeUpdate.select("text")
                    .style("fill-opacity", 1);

                // Переход выходящих узлов в новую позицию родителя.
                let nodeExit = node.exit().transition()
                    .duration(duration)
                    .attr("transform", function () {
                        return "translate(" + source.y + "," + source.x + ")";
                    })
                    .remove();

                nodeExit.select("circle")
                    .attr("r", 0);

                nodeExit.select("text")
                    .style("fill-opacity", 0);

                // Обновление ссылок
                let link = svgGroup.selectAll("path.link")
                    .data(links, function (d) {
                        return d.target.id;
                    });

                link.enter().insert("path", "g")
                    .attr("class", "link")
                    .attr("d", function () {
                        let o = {
                            x: source.x0,
                            y: source.y0
                        };
                        return diagonal({
                            source: o,
                            target: o
                        });
                    });

                link.transition()
                    .duration(duration)
                    .attr("d", diagonal);

                // Переход выходящих узлов в новую позицию родителя.
                link.exit().transition()
                    .duration(duration)
                    .attr("d", function () {
                        let o = {
                            x: source.x,
                            y: source.y
                        };
                        return diagonal({
                            source: o,
                            target: o
                        });
                    })
                    .remove();

                // Сохраните старые позиции для перехода.
                nodes.forEach(function (d) {
                    d.x0 = d.x;
                    d.y0 = d.y;
                });
            }

            outer_update.current = update
            let svgGroup = baseSvg.append("g");

            root = graph;
            root.x0 = viewerHeight / 2;
            root.y0 = 0;

            update(root);
            centerNode(root);
        }

    }, [graph, dispatch])

    useEffect(() => {
        if (clusters.length === 0) {
            dispatch(getClusters())
                .then(clusters => {
                    if (clusters.length > 0) {
                        dispatch(getGraph(clusters[0].id))
                        setClusterId(clusters[0].id)
                    }
                })
        }
    }, [clusters, dispatch])

    const handleCluster = (evt) => {
        dispatch(getGraph(evt.target.value))
        setClusterId(evt.target.value)
    }

    const handleClickRename = () => {
        rename_node();
    }

    const handleClickCreate = () => {
        create_node();
    }

    const handleOpenComment = () => {
        setIsOpenComment(true)
    }

    const handleCloseComments = () => {
        setIsOpenComment(false)
    }

    const handleDate = (evt) => {
        setDate(evt.target.value)
    }

    const handleLoadDate = () => {
        dispatch(getGraph(clusterId, date))
    }

    return (
        <>
            <div className="select-cluster-block">
                <div>
                    <label htmlFor="cluster">Выберите кластер</label>
                    <select id="cluster" value={clusterId} onChange={handleCluster}>
                        {clusters.map(cluster => {
                            return (
                                <option key={cluster.id} value={cluster.id}>{cluster.name}</option>
                            )
                        })}
                    </select>
                </div>
                <div className="comment-date-block">
                    <label htmlFor="history-date">Загрузить данные по дате</label>
                    <input type="date" id="history-date" value={date} onChange={handleDate}/>
                    <button className="comment-open-link" onClick={handleLoadDate} disabled={!date}>
                        Загрузить
                    </button>
                </div>
                <div className="comment-link-block">
                    <button className="comment-open-link" onClick={handleOpenComment}>
                        Комментарии
                    </button>
                </div>
            </div>
            {isOpenRenameNode &&
            <div className="modal">
                <h2>Переименовать узел</h2>
                <div className="close-btn" aria-label="Close" onClick={() => setIsOpenRenameNode(false)}>&#215;</div>
                <div>
                    <div className="row">
                        <div>
                            <label>Название узла
                                <input
                                    type="text"
                                    className="inputName"
                                    id='RenameNodeName'
                                    placeholder="Название узла"
                                    value={rename}
                                    onChange={e => setRename(e.target.value)}
                                />
                            </label>
                        </div>
                    </div>
                    <div className="row">
                        <div>
                            <button onClick={handleClickRename} className="button success">Переименовать</button>
                        </div>
                    </div>
                </div>
            </div>
            }

            {isOpenCreateNode &&
            <div className="modal">
                <h2>Создать узел</h2>
                <div className="close-btn" aria-label="Close" onClick={() => setIsOpenCreateNode(false)}>&#215;</div>
                <div>
                    <div className="row">
                        <div>
                            <label>Название узла
                                <input
                                    type="text"
                                    className="inputName"
                                    id='CreateNodeName'
                                    placeholder="Название узла"
                                    value={createName}
                                    onChange={e => setCreateName(e.target.value)}
                                />
                            </label>
                        </div>
                    </div>
                    <div className="row">
                        <div>
                            <div onClick={() => handleClickCreate()} className="button success">Создать</div>
                        </div>
                    </div>
                </div>
            </div>
            }

            <div id="tree-container"/>
            {isOpenComment &&
            <CommentList
                handleCloseComments={handleCloseComments}
            />
            }
        </>
    )
}

export default Dendrogram;
