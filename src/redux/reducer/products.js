import {
    SET_CLUSTERS,
    SET_GRAPH
} from "../constant/products";

const initialState = {
    clusters: [],
    graph: null
}

const productReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_CLUSTERS:
            return {
                ...state,
                clusters: action.clusters
            };
        case SET_GRAPH:
            return {
                ...state,
                graph: action.graph
            };
        default:
            return state;
    }
};

export default productReducer
