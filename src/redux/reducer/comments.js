import {
    SET_COMMENT,
    ADD_COMMENT,
    DELETE_COMMENT
} from "../constant/comments";

const initialState = {
    comments: [{
        id: 1,
        username: "Admin",
        date: "11.11.2011",
        message: "Тестовое сообщение 1"
    }, {
        id: 2,
        username: "Admin",
        date: "11.11.2011",
        message: "Тестовое сообщение 2"
    }, {
        id: 3,
        username: "Admin",
        date: "11.11.2011",
        message: "Тестовое сообщение 3"
    }, {
        id: 4,
        username: "Admin",
        date: "11.11.2011",
        message: "Тестовое сообщение 4"
    }, {
        id: 5,
        username: "Admin",
        date: "11.11.2011",
        message: "Тестовое сообщение 5"
    }, {
        id: 6,
        username: "Admin",
        date: "11.11.2011",
        message: "Тестовое сообщение 6"
    }, {
        id: 7,
        username: "Admin",
        date: "11.11.2011",
        message: "Тестовое сообщение 7"
    }, {
        id: 8,
        username: "Admin",
        date: "11.11.2011",
        message: "Тестовое сообщение 8"
    }, {
        id: 9,
        username: "Admin",
        date: "11.11.2011",
        message: "Тестовое сообщение 9"
    }, {
        id: 10,
        username: "Admin",
        date: "11.11.2011",
        message: "Тестовое сообщение 10"
    }, {
        id: 11,
        username: "Admin",
        date: "11.11.2011",
        message: "Тестовое сообщение 11"
    }, {
        id: 12,
        username: "Admin",
        date: "11.11.2011",
        message: "Тестовое сообщение 12"
    }, {
        id: 13,
        username: "Admin",
        date: "11.11.2011",
        message: "Тестовое сообщение 13"
    }, {
        id: 14,
        username: "Admin",
        date: "11.11.2011",
        message: "Тестовое сообщение 14"
    }, {
        id: 15,
        username: "Admin",
        date: "11.11.2011",
        message: "Тестовое сообщение 15"
    }, {
        id: 16,
        username: "Admin",
        date: "11.11.2011",
        message: "Тестовое сообщение 16"
    }, {
        id: 17,
        username: "Admin",
        date: "11.11.2011",
        message: "Тестовое сообщение 17"
    }, {
        id: 18,
        username: "Admin",
        date: "11.11.2011",
        message: "Тестовое сообщение 18"
    }, {
        id: 19,
        username: "Admin",
        date: "11.11.2011",
        message: "Тестовое сообщение 19"
    }, {
        id: 20,
        username: "Admin",
        date: "11.11.2011",
        message: "Тестовое сообщение 20"
    }, {
        id: 21,
        username: "Admin",
        date: "11.11.2011",
        message: "Тестовое сообщение 21"
    }, {
        id: 22,
        username: "Admin",
        date: "11.11.2011",
        message: "Тестовое сообщение 22"
    }, {
        id: 23,
        username: "Admin",
        date: "11.11.2011",
        message: "Тестовое сообщение 23"
    }, {
        id: 24,
        username: "Admin",
        date: "11.11.2011",
        message: "Тестовое сообщение 24"
    }],
    pageCount: 3
}

const commentsReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_COMMENT:
            const id = state.comments.findIndex(comment => comment.id === action.comment.id)
            state.comments[id] = action.comment
            return {
                ...state
            };
        case ADD_COMMENT:
            const date = new Date()
            const newComment = {
                id: state.comments[state.comments.length - 1].id + 1,
                message: action.message,
                username: "Admin",
                date: `${date.getDate()}.${date.getMonth()}.${date.getFullYear()}`
            }
            return {
                ...state,
                comments: [newComment, ...state.comments]
            };
        case DELETE_COMMENT:
            const commentId = state.comments.findIndex(comment => comment.id === action.commentId)
            state.comments.splice(commentId,1)
            return {
                ...state,
                pageCount: Math.ceil(state.comments.length / 10)
            };
        default:
            return state;
    }
};

export default commentsReducer
