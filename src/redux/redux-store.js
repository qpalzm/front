import {
    applyMiddleware, combineReducers, compose, createStore
} from 'redux';
import logger from 'redux-thunk';
import thunk from 'redux-thunk';
import productReducer from "./reducer/products";
import authReducer from "./reducer/auth";
import commentsReducer from "./reducer/comments";

const reducers = combineReducers({
    productData: productReducer,
    authData: authReducer,
    commentsData: commentsReducer
});

const middleware = applyMiddleware(thunk, logger);

const composeEnhancers = typeof window === 'object'
&& window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}) : compose;

const enhancer = composeEnhancers(
    middleware
);

const store = createStore(reducers, enhancer);

window.store = store;

export {store};
