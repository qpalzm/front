import {authAPI} from "../../api/authAPI";
import {loginUser} from "../action/auth";

export const getToken = (username, password) => dispatch => authAPI.getToken(username, password).then(async (response) => {
    localStorage.setItem("token", response["access_token"])
    window.location.href = '/graph';
    dispatch(loginUser(response))
})

export const logout = () => {
    localStorage.removeItem("token")
    window.location.href = "/login";
}
