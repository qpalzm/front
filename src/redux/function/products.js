import {productAPI} from "../../api/productAPI";
import {setClusters, setGraph} from "../action/products";
import {logout} from "./auth";

export const getClusters = () => dispatch => productAPI.getClusters()
    .then(response => {
        dispatch(setClusters(response))
        return response
    }).catch(error => {
        if (error.response.status === 401) {
            logout()
        }
    })

export const getGraph = (clusterId, date = null) => dispatch => productAPI.getGraph(clusterId, date)
    .then(response => {
        response = _clearData(response)
        dispatch(setGraph(response))
    }).catch(error => {
        if (error.status === 401) {
            logout()
        }
    })

export const setLink = (childId, parentId, childType) => dispatch => productAPI.setLink(childId, parentId, childType)
    .catch(error => {
        if (error.status === 401) {
            logout()
        }
    })

export const addProduct = (product) => dispatch => productAPI.addProduct(product)
    .then(response => response)
    .catch(error => {
        if (error.status === 401) {
            logout()
        }
    })
export const setProduct = (product) => dispatch => productAPI.setProduct(product)
    .catch(error => {
        if (error.status === 401) {
            logout()
        }
    })
export const deleteProduct = (productId) => dispatch => productAPI.deleteProduct(productId)
    .catch(error => {
        if (error.status === 401) {
            logout()
        }
    })


// Очистка данных под формат d3.js
const _clearData = (data) => {
    const newObj = {
        ...data,
        ID: data.id,
        children: data?.children ? data.children.map(item => _clearData(item)) : undefined
    }
    delete newObj.id
    return newObj
}
