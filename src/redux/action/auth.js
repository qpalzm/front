import {
    LOGIN_USER
} from "../constant/auth";

export const loginUser = (currentUser) => ({
    type: LOGIN_USER,
    currentUser
});
