import {
    SET_GRAPH,
    SET_CLUSTERS
} from "../constant/products";

export const setClusters = (clusters) => ({
    type: SET_CLUSTERS,
    clusters
});

export const setGraph = (graph) => ({
    type: SET_GRAPH,
    graph
});
