import {
    SET_COMMENT,
    ADD_COMMENT,
    DELETE_COMMENT
} from "../constant/comments";

export const setComment = (comment) => ({
    type: SET_COMMENT,
    comment
});

export const addComment = (message) => ({
    type: ADD_COMMENT,
    message
});

export const deleteComment = (commentId) => ({
    type: DELETE_COMMENT,
    commentId
});
