import React from 'react';
import {Switch, Route, BrowserRouter as Router, Redirect} from 'react-router-dom';
import Login from './components/Login';
import Dendrogram from "./components/Dendrogram";

function PrivateRoute ({component: Component, token, ...rest}) {
    return (
        <Route
            {...rest}
            render={(props) => token
                ? <Component {...props} />
                : <Redirect to={{pathname: '/login'}} />}
        />
    )
}

function App() {

    const token = localStorage.getItem('token');

    return (
        <Router>
            <Switch>
                <Route
                    path="/login"
                    render={() => !token ?
                        <Login/> :
                        <Redirect to="/graph" />
                    }/>
                <PrivateRoute token={token}  path="/graph" component={Dendrogram}/>
                <Route path="/" render={() => <Redirect to="/login" />} />
            </Switch>
        </Router>
    );

}

export default App;
